#
# Copyright (C) 2018-2019 The Google Pixel3ROM Project
# Copyright (C) 2024 The hentaiOS Project and its Proprietors
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

$(call inherit-product, vendor/gms/gms_mini.mk)

# product/app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    Photos \
    SideCar \
    Wallet \
    YouTubeMusicPrebuilt

# product/priv-app
#PRODUCT_PACKAGES += \
#    RecorderPrebuilt

# Extra apps are only inherit in full GMS
$(call inherit-product-if-exists, vendor/2by2-prebuilt-apps/apps.mk)
